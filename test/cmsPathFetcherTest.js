const expect = require("chai").expect;
const cmsPathFetcher = require("../src/cmsPathFetcher");
const fs = require("fs");

const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));

describe("Test the CMS path fetcher", () => {
    it("Should fetch basic CMS paths", async () => {
        const data = await cmsPathFetcher.cmsPathsFetcher(config.apis.cms, 'en');

        const requiredPaths = [
            '/education',
            '/blog',
            '/weather',
            '/weather/map/',
            '/archive',
            '/imprint'
        ];

        requiredPaths.forEach((p) => {
            expect(data.find((el) => el.path == p)).to.be.a('object');
        });
    });
});