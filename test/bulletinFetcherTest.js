const expect = require("chai").expect;
const bulletinFetcher = require("../src/bulletinFetcher");
const fs = require("fs");

const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));

describe("Test bulletin fetcher", () => {
    describe("Status fetching", () => {
        it("Fetches bulletin status for a single day", async () => {
            const response = await bulletinFetcher.bulletinStatusFetcher(config.apis.bulletin, '2019-01-01', '2019-01-01');
            expect(response).to.be.a('array');
            expect(response).to.have.lengthOf(1);
            expect(response[0].date.substr(0, 10)).to.equal('2019-01-01');
        });

        it("Fetches bulletin status for multiple days", async () => {
            const response = await bulletinFetcher.bulletinStatusFetcher(config.apis.bulletin, '2019-01-01', '2019-01-31');
            expect(response).to.be.a('array');
            expect(response).to.have.lengthOf(31);
        });
    });

    describe("Latest fetching", () => {
        it("Gets date of last bulletin", async () => {
            const response = await bulletinFetcher.bulletinLatestFetcher(config.apis.bulletin);
            expect(response).to.be.a('object');
            expect(response.date).to.be.a('string');
        });
    });
});