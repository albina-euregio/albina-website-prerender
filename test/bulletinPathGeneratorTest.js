const expect = require("chai").expect;
const date = require("../src/date");
const bulletinFetcher = require("../src/bulletinFetcher");
const bulletinPathGenerator = require("../src/bulletinPathGenerator");

const fs = require("fs");

const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
const bulletinLatestFetcher = () => bulletinFetcher.bulletinLatestFetcher(config.apis.bulletin);
const bulletinStatusFetcher = (startDateString, endDateString) => 
    bulletinFetcher.bulletinStatusFetcher(config.apis.bulletin, startDateString, endDateString);

describe("Test generated bulletin paths", () => {
    it("Starts at the latest bulletin", async () => {
        const latest = await bulletinLatestFetcher();
        const latestDate = latest.date.substr(0, 10);
        const gen = bulletinPathGenerator(bulletinStatusFetcher, bulletinLatestFetcher, config.minDate);
        const first = await gen.next();
    
        expect(first.value.path).to.equal('bulletin/' + latestDate);
    });

    it("Should return dates in descending order and stop at minDate", async () => {
        const gen = bulletinPathGenerator(bulletinStatusFetcher, bulletinLatestFetcher, config.minDate);

        let lastPath = (await gen.next()).value.path;

        const checkOrder = (path1, path2) => {
            const date1 = date.parseDateString(path1.substr(9));
            const date2 = date.parseDateString(path2.substr(9));
            return (date1 - date2) > 0;
        }

        const minDate = date.parseDateString(config.minDate);
        
        for await (const el of gen) {
            const p = el.path;
            expect(checkOrder(lastPath, p)).to.equal(true);
            expect(date.parseDateString(p.substr(9))).to.be.above(minDate);
            lastPath = p;
        }
    }).timeout(50000);
});
