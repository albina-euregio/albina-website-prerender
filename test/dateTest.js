const expect = require("chai").expect;
const date = require("../src/date");

describe("Test date functions", () => {
    describe("Parsing of date string", () => {
        it("Parses valid dates", () => {
            const validDates = ["2019-03-01", "2019-08-27"];
    
            validDates.forEach((d) => {
                expect(date.toDateString(date.parseDateString(d))).to.equal(d);
            });
        });
    
        it("Returns null on invalid dates", () => {
            const invalidDates = [" 2019-01-02", "2019-02-01T"];
    
            invalidDates.forEach((d) => {
                expect(date.parseDateString(d)).to.equal(null);
            });
        });
    });
    
    describe("Determines summer and winter time for a date", () => {
        it("Handles dates on winter to summer time switch", () => {
            expect(date.isSummerTime(date.parseDateString('2019-03-30'))).to.equal(false);
            expect(date.isSummerTime(date.parseDateString('2019-03-31'))).to.equal(false);
            expect(date.isSummerTime(date.parseDateString('2019-04-01'))).to.equal(true);
        });
    
        it("Handles dates on summer to winter time switch", () => {
            expect(date.isSummerTime(date.parseDateString('2019-10-26'))).to.equal(true);
            expect(date.isSummerTime(date.parseDateString('2019-10-27'))).to.equal(true);
            expect(date.isSummerTime(date.parseDateString('2019-10-28'))).to.equal(false);
        });
    });
    
    describe("Gets the correct predecessor date", () => {
        it("Works as expected", () => {
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-07-27')))).to.equal('2019-07-26');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-12-02')))).to.equal('2019-12-01');
        });
    
        it("Works on month boundaries", () => {
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-08-01')))).to.equal('2019-07-31');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-12-01')))).to.equal('2019-11-30');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-03-01')))).to.equal('2019-02-28');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2020-03-01')))).to.equal('2020-02-29');
        });
        
        it("Works on year boundaries", () => {
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-01-01')))).to.equal('2018-12-31');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2020-01-01')))).to.equal('2019-12-31');
        });
    
        it("Works on winter to summer time switch", () => {
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-03-31')))).to.equal('2019-03-30');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-04-01')))).to.equal('2019-03-31');
        });
    
        it("Works on summer to winter time switch", () => {
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-10-27')))).to.equal('2019-10-26');
            expect(date.toDateString(date.getPredDate(date.parseDateString('2019-10-28')))).to.equal('2019-10-27');
        });
    });
});