const expect = require("chai").expect;
const blogPostFetcher = require("../src/blogPostFetcher");
const blogPathGenerator = require("../src/blogPathGenerator");
const fs = require("fs");

const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));

const fetcher = () => blogPostFetcher.blogPostFetcher(config.apis.blog, config.apiKeys.google, config.blogs);

describe("Test generation of blog paths", () => {
    it("Should generate a non-empty list of blog paths in descending order", async () => {
        const gen = blogPathGenerator(fetcher);
        let lastPost = (await gen.next()).value;

        expect(lastPost).to.be.a('object');
        console.log(lastPost.path);
        
        for await(const post of gen) {
            const d1 = Date.parse(lastPost.lastModified);
            const d2 = Date.parse(post.lastModified);

            expect(d1).to.be.above(d2);
            console.log(post.path);
            lastPost = post;
        }
    }).timeout(50000);
});