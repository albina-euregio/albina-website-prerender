#!/usr/bin/env node

/* usage: node index.js [urls...] */
const fs = require('fs');
const yargs = require('yargs');
const sm = require('sitemap');

const cmsPathFetcher = require('./src/cmsPathFetcher');
const cmsPathGenerator = require('./src/cmsPathGenerator');
const bulletinFetcher = require('./src/bulletinFetcher');
const bulletinPathGenerator = require('./src/bulletinPathGenerator');
const blogPostFetcher = require('./src/blogPostFetcher');
const blogPathGenerator = require('./src/blogPathGenerator');

/* Load configuration */
const config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
if(!config) {
  console.err('Configuration file not found');
  exit(1);
}

const languages = Object.keys(config.baseURL);

async function getPaths(all = false) {
  const paths = [];

  const shouldRenderPath = (p) => {
    const f = config.outputDirectory + p.path;
    return all 
      || !fs.existsSync(f)
      || Date.parse(p.lastModified) > fs.statSync(f).mtimeMs;
  };

  const cmsPaths = cmsPathGenerator(
    (lang) => cmsPathFetcher.cmsPathsFetcher(config.apis.cms, lang),
    languages
  );
  const bulletinPaths = bulletinPathGenerator(
    (startDate, endDate) => bulletinFetcher.bulletinStatusFetcher(config.apis.bulletin, startDate, endDate),
    () => bulletinFetcher.bulletinLatestFetcher(config.apis.bulletin),
    config.minDate
  );
  const blogPaths = blogPathGenerator(
    () => blogPostFetcher.blogPostFetcher(config.apis.blog, config.apiKeys.google, config.blogs)
  );

  for await (const p of cmsPaths) {
    if(p.path != '/bulletin' && shouldRenderPath(p)) {
      paths.push(p);
    }
  }

  // add /bulletin/latest with timestamp of most recent bulletin
  const firstBulletin = (await bulletinPaths.next()).value;
  if(shouldRenderPath(firstBulletin)) {
    paths.push({
      path: '/bulletin/latest',
      lastModified: firstBulletin.lastModified
    });

    paths.push(firstBulletin);
  }

  for await (const p of bulletinPaths) {
    if(shouldRenderPath(p)) {
      paths.push(p);
    } else {
      break;
    }
  }

  for await (const p of blogPaths) {
    if(shouldRenderPath(p)) {
      paths.push(p);
    } else {
      break;
    }
  }

  return paths;  
}

function list(all = false) {
  (async () => {
    const paths = await getPaths();
    paths.map((p) => p.path).forEach((str) => {
      console.log(str);
    });
  })();
}

function render(all = false) {
  console.log('Not implemented');
}

function sitemap(out = null) {
  const makeUrl = (path, lang) => {
    const base = config.baseURL[lang];
    return base + (base.endsWith('/') 
      ? path.replace(/^\//, '')
      : path);
  };

  (async () => {
    const paths = await getPaths(true);
    const urls = paths.map((p) => {
      return {
        url: makeUrl(p.path, "en"),
        lastmodISO: p.lastModified,
        links: [
          { lang: "de", url: makeUrl(p.path, "de") },
          { lang: "it", url: makeUrl(p.path, "it") }
        ]
      }
    });

    const sitemap = sm.createSitemap({
      urls: urls,
    });

    if(out) {
      fs.writeFileSync(out, sitemap.toString());
    } else {
      console.log(sitemap.toString());
    }
  })();
}

yargs
  .scriptName("albina-prerender")
  .usage('$0 <cmd> [args]')
  .command('render [--all]', 'Run rendering of new and updated content.', (yargs) => {
    yargs.positional('all', {
      describe: 'Force rendering of all content',
      type: 'boolean'
    })
  }, (argv) => render(argv.all))
  .command('sitemap', 'Create a sitemap in XML format', (yargs) => {
    yargs.positional('out', {
      describe: 'Output file for the sitemap - defaults to stdout',
      type: 'string'
    })
  }, (argv) => sitemap(argv.out))
  .command('list [--all]', 'Get a list of new and updated paths', (yargs) => {
    yargs.positional('all', {
      describe: 'List all paths', 
      type: 'boolean'
    })
  }, (argv) => list(argv.all))
  .help()
  .argv;


// /*
//  * Create a list of paths that need to be rendered.
//  */
// async function checkPaths(forceRerendering = false) {
//   // all checks will return a path => lastModified hashtable
//   //const paths = await Promise.all([blogCheck(), bulletinCheck(), cmsCheck()]);
//   paths = [];

//   if(!forceRerendering) {
//     // toto check timestamp of date
//     return [];
//   }

//   return paths.reduce((acc, path) => acc.push(Object.keys(path)), []);
// }



// async function main(args) {
//   const pathsToRender = (args.length > 2) ? args.slice(2) : await checkPaths();

//   if(pathsToRender.length > 0) {
//     await renderer.render(pathsToRender);
//   }
// }

// (async () => {
//   await main(process.argv);
// })();
