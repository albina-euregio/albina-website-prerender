const fetcher = require('./fetcher');
const date = require('./date');

function makeDateParam(dateString) {
    const parsedDate = date.parseDateString(dateString);
    const time = date.isSummerTime(parsedDate) ?  '22:00:00' : '23:00:00';
    const d = date.getPredDate(parsedDate);

    return date.toDateString(d) + 'T' + time + 'Z';
}

async function bulletinStatusFetcher(apiBaseUrl, startDate, endDate) {
    const params = {
        startDate: makeDateParam(startDate),
        endDate: makeDateParam(endDate)
    };

    const url = apiBaseUrl 
        + 'bulletins/status?' 
        + Object.keys(params).map((p) => 
            p + '=' + encodeURIComponent(params[p])
        ).join('&');

    return await fetcher.fetch(url);
}

async function bulletinLatestFetcher(apiBaseUrl) {
    const url = apiBaseUrl + 'bulletins/latest';

    return await fetcher.fetch(url);
}

module.exports ={
    bulletinStatusFetcher, 
    bulletinLatestFetcher
};