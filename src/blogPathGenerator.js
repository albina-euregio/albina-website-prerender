async function* blogPathGenerator(blogFetcher) {
    const rawLists = await blogFetcher();
    const data = [];

    rawLists.forEach((blog) => {
        const basePath = '/blog/' + blog.name + '/';
        blog.list.forEach((post) => {
            const publishedDate = Date.parse(post.published);
            const updatedDate = Date.parse(post.updated);
            const timestamp = (updatedDate && updatedDate > publishedDate) ? updatedDate : publishedDate;

            data.push({
                timestamp: timestamp,
                path: basePath + post.id,
                lang: blog.lang
            });
        });
    });

    data.sort((a, b) => b.timestamp - a.timestamp);

    // TODO stop at config.minDate

    yield* data.map((el) => {
        const lastModified = (new Date(el.timestamp)).toISOString();
        return {
            path: el.path,
            lang: el.lang,
            lastModified: lastModified
        };
    });
}

module.exports = blogPathGenerator;