const fetcher = require('./fetcher');

async function fetchPostList(apiBaseUrl, apiKey, blogConfig) {
    const url = apiBaseUrl + blogConfig.params.id + '/posts?key=' + apiKey;
    const data = await fetcher.fetch(url);

    return {
        lang: blogConfig.lang,
        name: blogConfig.name,
        list: data.items
    };
}

async function blogPostFetcher(apiUrl, apiKey, blogs) {
    const posts = await Promise.all(blogs.map((cfg) => fetchPostList(apiUrl, apiKey, cfg)));

    return posts;
}

module.exports = {
    blogPostFetcher
}