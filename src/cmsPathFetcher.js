const fetcher = require('./fetcher');

async function cmsPathsFetcher(apiBaseUrl, lang) {
    const params = [
        'link',
        'changedAt'
    ];

    const url = apiBaseUrl 
        + ((lang == 'en') ? '' : lang + '/')
        + 'api/menuLinks?fields[menuLinks]='
        + encodeURIComponent(params.join(','))
        + '&sort=weight';

    const rawData = await fetcher.fetch(url);

    return rawData.data
        .map((el) => el.attributes)
        .filter((el) => el.link.match(/^internal:/))
        .map((el) => { return {
            'path': el.link.substr(9), // remove "internal:" prefix
            'lastModified': el.changedAt
        }});
}

module.exports = {
    cmsPathsFetcher
}