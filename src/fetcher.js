const axios = require('axios');

async function fetch(url, type="JSON") {
    const response = (await axios.get(url).then((res) => res.data));

    return response;
}

async function fetchMultiple(urls, type="JSON") {
    const response = (await axios.all(urls.map((url) => axios.get(url))));

    return response.map((r) => r.data);
}

module.exports = {
    fetch,
    fetchMultiple
};