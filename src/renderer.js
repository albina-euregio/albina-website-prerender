const puppeteer = require('puppeteer');
const { PendingXHR } = require('pending-xhr-puppeteer');
const fs = require('fs');

/*
 * Store the static html to a file. The file is determined by the language
 * code and the url path.
 */
function storeRenderedContent(outputDir, lang, urlPath, html) {
    const path = lang + urlPath;
  
    /* Create any intermediate directories that are missing */
    const intermediateDirs = path.split('/').slice(0, -1).reduce((acc, el) => {
      acc.push(acc[acc.length - 1] + '/' + el);
      return acc;
    }, [outputDir]);
  
    intermediateDirs.forEach((dir) => {
      if(!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    });
  
    /* Create file */
    fs.writeFileSync(outputDir + path, html);
  }
  
  
  async function render(pathsToRender, baseURLs, outputDir) {
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    for(const path of pathsToRender) {
      for(const lang of Object.keys(baseURLs)) {
        // avoid double slashes in URL path, or otherwise HTML meta tags might not be 
        // set correctly by the webapp
        const url = baseURLs[lang] +
          ( baseURLs[lang].endsWith('/')
            ? path.replace(/^\//, '')
            : path
          );
  
        console.log(url);
        const page = await browser.newPage();
        const pendingXHR = new PendingXHR(page);
        await page.goto(url, {waitUntil: "networkidle0"});
        await pendingXHR.waitForAllXhrFinished();
        const title = await page.evaluate(() => document.title);
        const html = await page.content();
  
        storeRenderedContent(outputDir, lang, path, html);
      }
    };
    browser.close();
  }


  module.exports = {
      render
  }
  