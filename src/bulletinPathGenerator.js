const date = require('./date');

const positiveStatus = ["published", "republished", "resubmitted"];

async function* bulletinPathGenerator(bulletinStatusFetcher, bulletinLatestFetcher, minDateString) {
    const fetchMonthStatus = async (endDate) => {
        const endDateString = date.toDateString(endDate);
        const startDateString = endDateString.replace(/^(\d{4}-\d{2})-\d{2}/, "$1-01");
        const st = await bulletinStatusFetcher(startDateString, endDateString);

        const status = {};
        st.forEach((el) => {
            status[el.date.substr(0,10)] = el.status;
        })
        return status;
    };

    const hasBulletin = (statusArray, d) => {
        const dateString = date.toDateString(d);
        return statusArray[dateString] 
            ? (positiveStatus.indexOf(statusArray[dateString]) >= 0) 
            : false;
    };

    const latestBulletin = await bulletinLatestFetcher();

    if(latestBulletin && latestBulletin.date.match(/^\d{4}-\d{2}-\d{2}/)) {
        const latestDateStr = latestBulletin.date.substr(0, 10);
        let d = date.parseDateString(latestDateStr);

        let status = await fetchMonthStatus(d);
        const endDate = date.parseDateString(minDateString);

        while(d > endDate) {
            if(hasBulletin(status, d)) {
                // TODO: fetch publication time for bulletins with status 
                // "republished" or "resubmitted"
                const p = {
                    path: '/bulletin/' + date.toDateString(d),
                    lastModified: date.toDateString(date.getPredDate(d)) + 'T16:10:00Z'
                }
                yield p;
            }

            const reloadStatus = (d.getDate() == 1);
            
            d = date.getPredDate(d);
            if(reloadStatus) {
                status = await fetchMonthStatus(d);
            }
        }
    }
}

module.exports = bulletinPathGenerator;