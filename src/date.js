function isSummerTime(date) {
    // NOTE: getTimezoneOffset gives negative values for timezones east of GMT!
    const summerTimeOffset = (() => {
      const jan = new Date(date.getFullYear(), 0, 1);
      const jul = new Date(date.getFullYear(), 6, 1);
      return -Math.min(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    })();
  
    return (-date.getTimezoneOffset()) >= summerTimeOffset;
}

function getPredDate(date) {
    const candidate = new Date(date.valueOf() - 1000 * 60 * 60 * 24);
    if(isSummerTime(date) && !isSummerTime(candidate)) {
        return new Date(date.valueOf() - 1000 * 60 * 60 * 23);
    } else if(!isSummerTime(date) && isSummerTime(candidate)) {
        return new Date(date.valueOf() - 1000 * 60 * 60 * 25);
    }
    return candidate;
}

function parseDateString(dateStr) {
    const dateMatch = dateStr.match(/^(\d{4})-(\d{2})-(\d{2})$/);
    if(dateMatch) {
        return new Date(dateMatch[1], parseInt(dateMatch[2])-1, dateMatch[3], 0, 0, 0);
    }
    return null;
}

function toDateString(date) {
    const pad = (d) => (d < 10) ? "0" + d : d;
    
    if (date) {
        return (
            date.getFullYear() +
            "-" +
            pad(date.getMonth() + 1) +
            "-" +
            pad(date.getDate())
        );
    }
    return "";
}

module.exports = {
    isSummerTime, 
    getPredDate, 
    parseDateString,
    toDateString
};