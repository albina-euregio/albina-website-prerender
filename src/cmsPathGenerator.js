async function* cmsPathGenerator(cmsPathFetcher, languages)  {
    /*
     * Fetch cms paths for all three languages in parallel and yield
     * the most recent modification timestamp of any translation.
     */
    const rawData = (await Promise.all(languages.map((lang) => cmsPathFetcher(lang))));

    const modificationDates = {};
    rawData.reduce((acc, el) => acc.concat(el), []).forEach((el) => {
        if(!modificationDates[el.path]) {
            modificationDates[el.path] = el.lastModified
        } else {
            const d1 = Date.parse(modificationDates[el.path]);
            const d2 = Date.parse(el.lastModified);

            if(d2 > d1) {
                modificationDates[el.path] = el.lastModified
            }
        }
    });

    yield* Object.keys(modificationDates).map((path) => {
        return {
            path: path,
            lastModified: modificationDates[path]
        };
    })
}

module.exports = cmsPathGenerator;